# -*- coding: utf-8 -*-

### using python3
# pip install pyOpenSSL
# pip install ndg-httpsclient
# pip install pyasn1
# pip install lxml
# brew install libxml2
# brew install libxslt
# brew link libxml2 --force
# brew link libxslt --force
# pip install beautifulsoup4
# pip install cssselect
# pip install sqlite3

# brew install tor
# pip install pysocks
### need to open another terminal key 'tor' to run tor

import socks
import socket
socks.setdefaultproxy(proxy_type=socks.PROXY_TYPE_SOCKS5, addr="127.0.0.1", port=9050)
socket.socket = socks.socksocket

from bs4 import BeautifulSoup
import cssselect
import itertools
import lxml.html
import sqlite3
import time
import urllib3.contrib.pyopenssl
import os

# import threading as td
# import multiprocessing as mp

urllib3.contrib.pyopenssl.inject_into_urllib3()
urllib3.disable_warnings()
http = urllib3.PoolManager()

conn = sqlite3.connect('datas.sqlite')
cur = conn.cursor()
cur.execute('''CREATE TABLE IF NOT EXISTS PTT (
	url TEXT,
	日期 TEXT,
	作者 TEXT,
	標題 TEXT,
	內文 TEXT,
	看板名稱 TEXT)''')
cur.execute('''CREATE TABLE IF NOT EXISTS PTT_index (
	index_id INTEGER,
	num INTEGER)''')


def change_ip():
	os.system("killall -HUP tor")
	global http
	http = urllib3.PoolManager()
	ip_chack = http.request('GET', 'http://icanhazip.com')
	ip_soup = BeautifulSoup(ip_chack.data, "lxml")
	ip_tree = lxml.html.fromstring(str(ip_soup))
	ip = ip_tree.cssselect('p')[0]
	ip = ip.text_content()
	print(ip)


def download_urls(url):
	print('Downloading: ', url)
	response = http.request('GET', url)
	soup = BeautifulSoup(response.data, "lxml")
	if '500 - Internal Server Error' in str(soup):
		return None
	else:
		tree = lxml.html.fromstring(str(soup))
		links = tree.cssselect('div.title a')
		if links == []:
			change_ip()
			download_urls(url)
		for link in links:
			seed = 'https://www.ptt.cc' + link.get('href')
			print(seed)
			cur.execute("SELECT url FROM PTT WHERE url = ?", (seed,))
			data=cur.fetchone()
			if data==None:
				cur.execute('''INSERT INTO PTT (url, 日期, 作者, 標題, 內文, 看板名稱) VALUES (?,?,?,?,?,?)''', (seed,None,None,None,None,None))
				conn.commit()
			else:
				print('exist')
				pass
		return 'Worked'


def download_datas(url):
	print('Downloading: ', url)
	response = http.request('GET', url)
	soup = BeautifulSoup(response.data, "lxml")
	tree = lxml.html.fromstring(str(soup))
	try:
		date = tree.cssselect('div.article-metaline > span.article-meta-value')[2]
		date = date.text_content()
		author = tree.cssselect('div.article-metaline > span.article-meta-value')[0]
		author = author.text_content()
		title = tree.cssselect('div.article-metaline > span.article-meta-value')[1]
		title = title.text_content()
		content = tree.cssselect('div#main-content')[0]
		content = content.text_content()
		board = tree.cssselect('div.article-metaline-right > span.article-meta-value')[0]
		board = board.text_content()
		return [date, author, title, content, board]
	except:
		try:
			content = tree.cssselect('div#main-content')[0]
			content = content.text_content()
			board = tree.cssselect('div#topbar > a.board')[0]
			board = board.text_content()
			board = board.split(' ')[1]
			return [None, None, None, content, board]
		except:
			change_ip()
			download_datas(url)


if __name__ == '__main__':
	if cur.execute("SELECT num FROM PTT_index").fetchall() == []:
		cur.execute('''INSERT INTO PTT_index (index_id,num) VALUES (1,1)''')
		conn.commit()
	index_num = cur.execute("SELECT num FROM PTT_index").fetchall()[0][0]

	# num_cpus = mp.cpu_count()
	# print 'Starting {} processes'.format(num_cpus)
	# pool = mp.Pool(processes=num_cpus)

	for cc in itertools.count(index_num):
		url = 'https://www.ptt.cc/bbs/Salary/index%d.html' % cc
		html = download_urls(url)
		if html==None:
			break
		else:
			# time.sleep(1)
			sql = ''' UPDATE PTT_index
					  SET num = ?
					  WHERE index_id = ?'''
			sql_values = (cc,1)
			cur.execute(sql, sql_values)
			conn.commit()
		if cc%10==0:
			change_ip()


	seeds = cur.execute('''SELECT url FROM PTT WHERE 看板名稱 IS NULL''').fetchall()
	for idx,seed in enumerate(seeds):
		while True:
			try:
				url = (', '.join(seed))
				datas = download_datas(url)
				sql = ''' UPDATE PTT
			              SET 日期 = ? ,
			          		  作者 = ? ,
			          		  標題 = ? ,
							  內文 = ? ,
							  看板名稱 = ?
			              WHERE url = ?'''
				sql_values = (datas[0],datas[1],datas[2],datas[3],datas[4],url)
				cur.execute(sql, sql_values)
				conn.commit()
				break
			except:
				change_ip()
		# time.sleep(1)
		if idx%10==0:
			change_ip()
