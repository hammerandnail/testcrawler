import itertools
from collections import Counter

count = []
urls = [
	"http://www.google.com/a.txt",
	"http://www.google.com.tw/a.txt",
	"http://www.google.com/download/c.jpg",
	"http://www.google.co.jp/a.txt",
	"http://www.google.com/b.txt",
	"https://facebook.com/movie/b.txt",
	"http://yahoo.com/123/000/c.jpg",
	"http://gliacloud.com/haha.jpg",
]

for url in urls:
	u=url.split('/')
	count.append(u[-1])

counted = Counter(count)

cc=1
while cc<=3:
	print(max(counted, key=counted.get) + ' ' + str(counted.get(max(counted, key=counted.get))))
	del counted[max(counted, key=counted.get)]
	cc+=1
