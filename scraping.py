# -*- coding: utf-8 -*-

### using python3
# pip install pyOpenSSL
# pip install ndg-httpsclient
# pip install pyasn1
# pip install lxml
# brew install libxml2
# brew install libxslt
# brew link libxml2 --force
# brew link libxslt --force
# pip install beautifulsoup4
# pip install cssselect
# pip install sqlite3

from bs4 import BeautifulSoup
import cssselect
import itertools
import lxml.html
import requests
import sqlite3
import time
import urllib3.contrib.pyopenssl

urllib3.contrib.pyopenssl.inject_into_urllib3()
urllib3.disable_warnings()
http = urllib3.PoolManager()

seeds = []

conn = sqlite3.connect('datas.sqlite')
cur = conn.cursor()
cur.execute('''CREATE TABLE IF NOT EXISTS PTT (
	url TEXT, 
	日期 TEXT, 
	作者 TEXT, 
	標題 TEXT, 
	內文 TEXT,
	看板名稱 TEXT)''')

def get_links(url):
	print('Downloading: ', url)
	response = http.request('GET', url)
	soup = BeautifulSoup(response.data)
	if '500 - Internal Server Error' in str(soup):
		return None
	else:
		tree = lxml.html.fromstring(str(soup))
		links = tree.cssselect('div.title a')
		for link in links:
			seed = 'https://www.ptt.cc' + link.get('href')
			print(seed)
			cur.execute("SELECT url FROM PTT WHERE url = ?", (seed,))
			data=cur.fetchone()
			if data==None:
				seeds.append(seed)
			else:
				print('exist')
				pass

		return 'Worked'


def download_datas(url):
	print('Downloading: ', url)
	response = http.request('GET', url)
	soup = BeautifulSoup(response.data)
	tree = lxml.html.fromstring(str(soup))
	try:
		date = tree.cssselect('div.article-metaline > span.article-meta-value')[2]
		date = date.text_content()
		author = tree.cssselect('div.article-metaline > span.article-meta-value')[0]
		author = author.text_content()
		title = tree.cssselect('div.article-metaline > span.article-meta-value')[1]
		title = title.text_content()
		content = tree.cssselect('div#main-content')[0]
		content = content.text_content()
		board = tree.cssselect('div.article-metaline-right > span.article-meta-value')[0]
		board = board.text_content()
		return [date, author, title, content, board]
	except:
		content = tree.cssselect('div#main-content')[0]
		content = content.text_content()
		board = tree.cssselect('div#topbar > a.board')[0]
		board = board.text_content()
		board = board.split(' ')[1]
		return [None, None, None, content, board]


for cc in itertools.count(1):
	url = 'https://www.ptt.cc/bbs/Salary/index%d.html' % cc
	html = get_links(url)
	if html==None:
		break
	else:
		time.sleep(1)

for seed in seeds:
	datas = download_datas(seed)
	cur.execute('''INSERT INTO PTT (url, 日期, 作者, 標題, 內文, 看板名稱) VALUES (?,?,?,?,?,?)''', (seed,datas[0],datas[1],datas[2],datas[3],datas[4]))
	conn.commit()
	time.sleep(1)

